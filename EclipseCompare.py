#*******************************************************************************
#						 Eclipse Explorer - v0.07 (CFHT VERSION!!) - b.j.clark@keele.ac.uk
#*******************************************************************************

# Libraries --------------------------------------------------------------------
from standard import *
from uncertainties import unumpy
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata
import matplotlib.colorbar
import scipy.signal
import scipy.stats
import commands
import subprocess
import time as tcon
import sys
import multiprocessing
import signal
import pandas
import astropysics.phot

debug = False

# User Variables ---------------------------------------------------------------
star_dir = "/media/bjc/Processing/nu_w48/phots_4/pre_filt/fw_None/" #output folder from phot code
output_dir = "/media/bjc/Processing/nu_w48/phots_4/runs/run2_backup/" #output folder

target = 1 #star number of target (usually 1)
sig_cut = 6 #sigma cut level
phot_output = "pltsecl06_01.dat" #the photometry file output by the mcmc for the specific eclipse
phot_mod = "pltsmod06.dat"
channel_name = "ch6.csv" #output csv file containing eclipse depth
rms_cut = 15 #% above minimum rmsbeta for LCs
script_dir = "/home/bjc/desk/cfht/lcfit/main/mcmc_inputs/" #path with mcmc inputs
col_no = 1 #output csv eclipse depth channel
mcmc_name = "mcmc_2016_occ2" #mcmc input scipt name
mcmc_path = script_dir+mcmc_name
mcmc_inp = script_dir+"linT.inp" #base mcmc input file
transit = False #is this a transit(True) or occultation(False)
max_stars=15 #maximum number of stars included in the final lightcurve

# Functions --------------------------------------------------------------------
def getstar(x,ap):
	#Get time,flux information from the aperture photometry file
	#x - number of star ie. 1 for star1_12.0px.dat
	#ap - aperture size ie. 12.0 for star1_12.0px.dat
	ap = "%.2f"%float(ap)
	star_dat = readfile(star_dir+"star"+str(x)+"/star"+str(x)+"_"+ap+"px.dat")
	star_flux = unumpy.uarray(star_dat[1],star_dat[2])
	star_time = star_dat[0]
	return (star_time,star_flux)

def signal_handler(signal, frame):
	print('WAIT: Clearing any MCMC runs..')
	commands.getoutput("kill -9 `ps -ef | grep "+mcmc_name+" | grep -v grep | awk '{print $2}'`")
	print "Done, exiting.."
	sys.exit(0)

def sigma(x):
	x = numpy.asarray(x,dtype="float")
	return numpy.std(x[~numpy.isnan(x)])

def beta_red(sig_n,sig_1,N,M):
	B_red = (float(sig_n)/float(sig_1)) * numpy.sqrt( (float(N)* (float(M) - 1) ) / float(M) )
	return B_red

def time_bin(time,arr,binsize=5):
	#time in days
	#arr = y values to be binned
	#binsize in minutes
	twidth_days = numpy.amax(time) - numpy.amin(time)
	twidth_mins = twidth_days * 24 *60
	nbins = int(twidth_mins/binsize)
	if nbins == 0:
		return (numpy.nan,numpy.nan)
	biny,binx,binloc = scipy.stats.binned_statistic(time, res, statistic='mean', bins=nbins)
	nbins = numpy.amax(binloc)
	Nbin = int(len(time)/nbins)
	binx = 0.5*(binx[:-1] + binx[1:])
	return (binx,biny,nbins,Nbin)

def lcbin(time,flux,binsize):
	twidth_days = numpy.amax(time) - numpy.amin(time)
	twidth_mins = twidth_days * 24 *60
	nbins = int(twidth_mins/binsize)
	tsplit = numpy.array_split(time,nbins)
	bin_t = [numpy.mean(x) for x in tsplit ]
	bin_f = [numpy.mean(x) for x in numpy.array_split(flux,nbins)]
	bin_e = [numpy.std(x)*(1/numpy.sqrt(len(x))) for x in numpy.array_split(flux,nbins)]
	return (bin_t,bin_f,bin_e)

def rms_beta(time,res,bs=10,be=30,br=20):
	time,res = zip(*sorted(zip(time,res)))
	sig_white = sigma(res)
	beta_rs = []
	for binsz in numpy.linspace(bs,be,br):
		bt,br,nbins,Nbin = time_bin(time,res,binsz)
		sig_red = sigma(br)
		beta_rs.append(beta_red(sig_red,sig_white,Nbin,nbins))
	B_red = numpy.amax(beta_rs)
	return sig_white * B_red * B_red


def grid(x, y, z, resX=100, resY=100):
	"Convert 3 column data to matplotlib grid"
	xi = linspace(min(x), max(x), resX)
	yi = linspace(min(y), max(y), resY)
	Z = griddata(x, y, z, xi, yi,interp='linear')
	X, Y = meshgrid(xi, yi)
	return X, Y, Z


def rectangle(axs,x,y,c="k"):
	import matplotlib.patches as patches
	axs.add_patch(
	patches.Rectangle(
		(x, y),	 # (x,y)
		1.,			# width
		1.,			# height
		facecolor=c,
		alpha=1.,
		hatch=None,
		edgecolor=c,
		linewidth=1.,
		linestyle='solid'
	))

def plot_rms(y,x,flux,fig1=False):
	x=nar(x)-.5
	y=nar(y)-.5
	if fig1==False:
		fig1 = plt.figure(figsize=(18,5))
	ax1 = fig1.add_axes([0.05, 0.575, 0.80, 0.375])
						# [x0	 y0	 w		h]
	fl = nar(flux)
	ma = median(fl)+(3*med_abs_dev(fl))
	norm = plt.Normalize(vmax=ma,vmin=min(flux))
	colors = plt.cm.hot(norm(flux))
	for i in range(0,len(z)):
		rectangle(ax1,x[i],y[i],colors[i])
	plt.axis(xmin=min(x),xmax=max(x),ymin=min(y),ymax=max(y))
	axis_ticks(ax1,xticks=(1,0.25),scale=0.2)
	plt.ylabel("Number of Stars")
	plt.xlabel("Aperture Size")
	ax2 = fig1.add_axes([0.9, 0.575, 0.02, 0.375])
	cb1 = matplotlib.colorbar.ColorbarBase(ax2, cmap=plt.cm.hot,
								norm=norm,
								orientation='vertical')
	cb1.set_label("% above minimum "+ r'$rms \times {\beta}^2$',labelpad=-75)
	return ax1,x,y,flux


def plot_ecl(y,x,flux,fig1=False):
	x=nar(x)-.5
	y=nar(y)-.5
	if fig1==False:fig1 = plt.figure(figsize=(18,5))
	ax1 = fig1.add_axes([0.05, 0.075, 0.80, 0.3750])
						# [x0	 y0	 w		h]
	fl = nar(flux)
	ma = median(fl)+(4*med_abs_dev(fl))
	mi = median(fl)-(4*med_abs_dev(fl))
	norm = plt.Normalize(vmax=ma,vmin=mi)
	colors = plt.cm.hot(norm(flux))
	for i in range(0,len(z)):
		rectangle(ax1,x[i],y[i],colors[i])
	plt.axis(xmin=min(x),xmax=max(x),ymin=min(y),ymax=max(y))
	axis_ticks(ax1,xticks=(1,0.25),scale=0.2)
	plt.ylabel("Number of Stars")
	plt.xlabel("Aperture Size")
	ax2 = fig1.add_axes([0.9, 0.075, 0.02, 0.375])
	cb1 = matplotlib.colorbar.ColorbarBase(ax2, cmap=plt.cm.hot,
								norm=norm,
								orientation='vertical')
	cb1.set_label("Eclipse Depth (%)",labelpad=-100)
	return ax1,x,y,flux

def contour(ax1,x,y,flux):
	flux_cntr = flux[:]
	x_cntr = x.tolist()
	y_cntr = y.tolist()

	#Pad for contour plot to edge
	x_pad_idx = numpy.where(x_cntr==min(numpy.unique(x_cntr)))[0]
	x_lo_pad = nar(x_cntr)[x_pad_idx] -.5
	yx_lo_pad = nar(y_cntr)[x_pad_idx]
	zx_lo_pad = nar(flux_cntr)[x_pad_idx]
	x_cntr = x_cntr+x_lo_pad.tolist()
	y_cntr = y_cntr + yx_lo_pad.tolist()
	flux_cntr = flux_cntr+ zx_lo_pad.tolist()
	
	x_pad_idx = numpy.where(y_cntr==min(numpy.unique(y_cntr)))[0]
	x_lo_pad = nar(x_cntr)[x_pad_idx]
	yx_lo_pad = nar(y_cntr)[x_pad_idx]-.5
	zx_lo_pad = nar(flux_cntr)[x_pad_idx]
	x_cntr = x_cntr+x_lo_pad.tolist()
	y_cntr = y_cntr + yx_lo_pad.tolist()
	flux_cntr = flux_cntr+ zx_lo_pad.tolist()

	flux_cntr= nar(flux_cntr)
	flux_cntr[numpy.where(flux_cntr<=rms_cut)] = 0
	flux_cntr[numpy.where(flux_cntr>rms_cut)] = 10
	X, Y, Z = grid(x_cntr, y_cntr, flux_cntr)

	CS = ax1.contour(X+.5, Y+.5, Z, 1,
					#[-1, -0.1, 0, 0.1],
					#alpha=0.5,
					colors="#00BCFA",
					linecolor="white",
					#cmap=plt.cm.hot,
					origin='lower')

def flux2mag(flux,flxerr):
	med = numpy.median(flux[~numpy.isnan(flux)])
	flux = flux/med
	flxerr = flxerr/med
	mag,merr = astropysics.phot.lum_to_mag(flux,0,1,flxerr)
	return (mag,merr)


# Main -------------------------------------------------------------------------
script_dir =	slash(os.path.dirname(os.path.realpath(__file__)))
output_dir = cmdir(output_dir)
rms_dir = cmdir(output_dir+"rms/")

#get list of all stars and apertures
stars = [int(x.strip("/").split("star")[-1]) for x in get_files(star_dir,typ="d")]
comps = stars[:]
del comps[comps.index(target)]
apertures = [x.split("_")[-1].split("px")[0] for x in get_files(star_dir+"star"+str(target)+"/",contains="star"+str(target)+"_")]
apertures = numpy.array(apertures,dtype="float")
apertures = apertures[numpy.where(apertures<30.01)]
apertures = ["%.2f"%ap for ap in apertures]

ngood = len(getstar(target,numpy.amax(nar(apertures)))[0])

#create initial lighcurves for targ/comp for every individual star/ap
init_lc_dir = cmdir(output_dir+"init_lcs/")
print "\nCreating lighcurves for each star and aperture.."
bar = pbar(len(apertures));cnt=0;
for ap in apertures:
	bar.update(cnt);cnt+=1;
	try:
		targt, targf = getstar(target,ap)
	except:
		continue
	for c in comps:
		this_path = init_lc_dir+"lc_st"+str(c)+"_ap"+str(ap)+"/"
		if os.path.exists(this_path+"quadT.lc"):continue;
		lcpath = cmdir(this_path)
		compt, compf = getstar(c,ap)
		compf[numpy.where(compf==0)]=numpy.nan
		lc = targf/compf
		t,f,e = targt, nar([x.n for x in lc]), nar([x.s for x in lc])
		if debug==False:
			if transit == False:
				if t[0] < 50000:
					t = t + 249999.5 #CFHT ONLY
				elif t[0] < 2400000:
					t = t + 2399999.5 #CFHT ONLY
			else:
				if t[0] > 2450000:
					t = t - 2450000.5 #CFHT ONLY
				elif t[0] > 50000:
					t = t - 49999.5 #CFHT ONLY
			filt = scipy.signal.medfilt(f,kernel_size=21)
			res = f/filt
			fme = numpy.median(res[~numpy.isnan(res)])
			fsi = med_abs_dev(res[~numpy.isnan(res)])
			if fsi == 0:
				os.rmdir(this_path)
				continue
			t = t[numpy.where((res<(fme+(sig_cut*fsi))) & (res>(fme-(sig_cut*fsi))))]
			e = e[numpy.where((res<(fme+(sig_cut*fsi))) & (res>(fme-(sig_cut*fsi))))]
			f = f[numpy.where((res<(fme+(sig_cut*fsi))) & (res>(fme-(sig_cut*fsi))))]
			e = e/median(f)
			f = f/median(f)
			if transit == True: f,e = flux2mag(f,e)
			if len(f) < (ngood/100)*90:
				os.rmdir(lcpath)
				print "ONLY", len(f),"/",ngood, "Points found - skipping"
				continue
		else:
			fme = numpy.median(f[~numpy.isnan(f)])
			fsi = med_abs_dev(f[~numpy.isnan(f)])
			t = t[numpy.where((f<(fme+(5*fsi))) & (f>(fme-(5*fsi))))]
			t = t + 2400000.5 #CFHT ONLY
			e = e[numpy.where((f<(fme+(5*fsi))) & (f>(fme-(5*fsi))))]
			f = f[numpy.where((f<(fme+(5*fsi))) & (f>(fme-(5*fsi))))]
			f = f/fme
			e = e/fme
		arrays_to_file([t,f,e,0,0],lcpath+"notr.lc")
		arrays_to_file([t,f,e,t,0],lcpath+"linT.lc")
		arrays_to_file([t,f,e,t,t*t],lcpath+"quadT.lc")
bar.finish()

#run mcmc's for initial lightcurves
signal.signal(signal.SIGINT, signal_handler)
print "\nRunning MCMC's for each lighcurve. This may take a while.."
main_dir = init_lc_dir
if main_dir.endswith("/"):main_dir=main_dir[:-1]
folds = get_files(init_lc_dir,typ="d")
bar = pbar(len(folds))
i = 0; nskip = 0;
nproc = multiprocessing.cpu_count()+2
if os.path.exists(rms_dir+"initial_lc_rms_beta.npy"):
	print "- Skipping MCMC analysis due to saved RMS data"
else:
	for i in range(0,len(folds)):
		while True == True:
			no_mcmcs = commands.getoutput("ps -ef | grep "+mcmc_name+" | grep -v grep | awk '{print $2}'")
			no_mcmcs = len(no_mcmcs.split("\n"))
			if no_mcmcs < nproc:
				break
			tcon.sleep(5)
		os.chdir(main_dir+"/"+folds[i])
		bar.update(i)
		if os.path.exists(main_dir+"/"+folds[i]+"mcmc.out"):
			nskip += 1
			continue
		subprocess.Popen("nohup "+mcmc_path+" < "+mcmc_inp+" 2> Log &", shell=True)
		i += 1
	bar.finish()
	if nskip != 0:
		print "WARNING:", nskip, "folders skipped!"

	while True == True:
		no_mcmcs = commands.getoutput("ps -ef | grep "+mcmc_name+" | grep -v grep | awk '{print $2}'")
		no_mcmcs = len(no_mcmcs.split("\n"))
		if no_mcmcs == 1:
			break
		tcon.sleep(5)

#calculate RMS beta for initial runs
print "\nCalculating RMS Beta Squared for initial lightcurves.."
if os.path.exists(rms_dir+"initial_lc_rms_beta.npy"):
	print "- Loading saved RMS data"
	data = numpy.load(rms_dir+"initial_lc_rms_beta.npy")
else:
	bar = pbar(len(folds)); cnt = 0;
	stats = []
	for f in folds:
		bar.update(cnt); cnt +=1;
		fl = slash(main_dir)+slash(f)+phot_output
		try:
			time = readfile(fl)[1]
			if transit == True:
				res = readfile(fl)[3]
			else:
				res = readfile(fl)[5]
			stats.append((rms_beta(time,res),f))
		except IndexError:
			print "ERROR: Cannot read output file in:", f
		except IOError:
			stats.append((9999999.,f))
			# input("STOP")
	bar.finish()
	stats = sorted(stats)
	stats = nar(stats)
	sortd = []
	for s in stats:
		# print s
		rms = s[0]
		star = s[1].split("_st")[1].split("_")[0]
		ap = s[1].split("_ap")[1].split("/")[0]
		sortd.append((star,ap,rms))
	data = nar(sortd)
	numpy.save(rms_dir+"initial_lc_rms_beta.npy",data)


#Rank by median rmsbeta
print "\nRanking stars..",
cols = ["star","ap","rms"]
df = pandas.DataFrame(columns=cols,data=data)
sort = []
for star in df.star.unique():
	star_dat = df.loc[df["star"]==star]
	sort.append((median(star_dat.rms), med_abs_dev(star_dat.rms),star))
sort = sorted(sort)
stars_ranked = []
for s in sort:
	stars_ranked.append("%d"%s[2])
stars_ranked = [int(x) for x in stars_ranked][:max_stars]
print "complete"

#create lightcurves with combined comparison stars based upon rmsbeta rank
print "\nCreating lightcurves with combined comparison stars.."
comb_lc_dir = cmdir(output_dir+"comb_lcs/")
bar = pbar(len(stars_ranked));cnt=0;
for no in range(1,len(stars_ranked)):
	bar.update(cnt); cnt += 1;
	comps = stars_ranked[:no]
	for ap in apertures:
		try:
			targt, targf = getstar(target,ap)
		except IndexError as e:
			# print target,ap
			# print e
			continue
		this_path = comb_lc_dir+"lc_"+str(len(comps))+"N_st"+str("+".join([str(x) for x in comps]))+"_ap"+str(ap)+"/"
		if os.path.exists(this_path+"quadT.lc"):continue
		lcpath = cmdir(this_path)
		comp_flux = []
		for c in comps:
			comp_flux.append(getstar(c,ap)[1])
		compf = numpy.mean(comp_flux,axis=0)
		compf[numpy.where(compf==0)]=numpy.nan
		lc = targf/compf
		t,f,e = targt, nar([x.n for x in lc]), nar([x.s for x in lc])
		if debug == False:
			if transit == False:
				if t[0] < 50000:
					t = t + 2449999.5 #CFHT ONLY
				elif t[0] < 2400000:
					t = t + 2399999.5 #CFHT ONLY
			else:
				if t[0] > 2450000:
					t = t - 2450000.5 #CFHT ONLY
				elif t[0] > 50000:
					t = t - 49999.5 #CFHT ONLY
			filt = scipy.signal.medfilt(f,kernel_size=21)
			res = f/filt
			fme = numpy.median(res[~numpy.isnan(res)])
			fsi = med_abs_dev(res[~numpy.isnan(res)])
			t = t[numpy.where((res<(fme+(sig_cut*fsi))) & (res>(fme-(sig_cut*fsi))))]
			e = e[numpy.where((res<(fme+(sig_cut*fsi))) & (res>(fme-(sig_cut*fsi))))]
			f = f[numpy.where((res<(fme+(sig_cut*fsi))) & (res>(fme-(sig_cut*fsi))))]
			e = e/median(f)
			f = f/median(f)
		else:
			fme = numpy.median(f[~numpy.isnan(f)])
			fsi = med_abs_dev(f[~numpy.isnan(f)])
			t = t[numpy.where((f<(fme+(5*fsi))) & (f>(fme-(5*fsi))))]
			t = t + 2400000.5 #CFHT ONLY
			e = e[numpy.where((f<(fme+(5*fsi))) & (f>(fme-(5*fsi))))]
			f = f[numpy.where((f<(fme+(5*fsi))) & (f>(fme-(5*fsi))))]
			f = f/fme
			e = e/fme
		if transit == True: f,e = flux2mag(f,e)
		if len(f) < (ngood/100)*90:
			os.rmdir(lcpath)
			continue
		arrays_to_file([t,f,e,0,0],lcpath+"notr.lc")
		arrays_to_file([t,f,e,t,0],lcpath+"linT.lc")
		arrays_to_file([t,f,e,t,t*t],lcpath+"quadT.lc")
bar.finish()

#Run mcmc's for combination lightcurves
print "\nRunning MCMC's for each best ranking combination lighcurve. This may take a while.."
main_dir = comb_lc_dir
folds = get_files(comb_lc_dir,typ="d")
bar = pbar(len(folds))
i = 0; nskip = 0;
nproc = multiprocessing.cpu_count()+2
if os.path.exists(rms_dir+"comb_lc_rms_beta.npy"):
	print "- Skipping MCMC analysis due to saved RMS data"
else:
	for i in range(0,len(folds)):
		while True == True:
			no_mcmcs = commands.getoutput("ps -ef | grep "+mcmc_name+" | grep -v grep | awk '{print $2}'")
			no_mcmcs = len(no_mcmcs.split("\n"))
			if no_mcmcs < nproc:
				break
			tcon.sleep(5)
		os.chdir(main_dir+"/"+folds[i])
		bar.update(i)
		if os.path.exists(main_dir+"/"+folds[i]+"mcmc.out"):
			nskip += 1
			continue
		subprocess.Popen("nohup "+mcmc_path+" < "+mcmc_inp+" 2> Log &", shell=True)
		i += 1
	bar.finish()
	if nskip != 0:
		print "WARNING:", nskip, "folders skipped!"

	while True == True:
		no_mcmcs = commands.getoutput("ps -ef | grep "+mcmc_name+" | grep -v grep | awk '{print $2}'")
		no_mcmcs = len(no_mcmcs.split("\n"))
		# print no_mcmcs
		if no_mcmcs == 1:
			break
		tcon.sleep(5)

#calculate RMS beta for combination runs
lc_folds = get_files(comb_lc_dir,typ="d")
print "\nCalculating RMS Beta Squared for combined lightcurves.."
rms_dir = cmdir(output_dir+"rms/")
if os.path.exists(rms_dir+"comb_lc_rms_beta.npy"):
	print "- Loading saved RMS data"
	data = numpy.load(rms_dir+"comb_lc_rms_beta.npy")
else:
	bar = pbar(len(lc_folds)); cnt = 0;
	stats = []
	for f in lc_folds:
		bar.update(cnt); cnt +=1;
		fl = comb_lc_dir+f+phot_output
		try:
			time = readfile(fl)[1]
		except IOError:
			stats.append((99999,f))
			continue
		if transit == True:
			res = readfile(fl)[3]
		else:
			res = readfile(fl)[5]
		stats.append((rms_beta(time,res),f))
	bar.finish()
	stats = sorted(stats)
	stats = nar(stats)
	sortd = []
	for s in stats:
		# print s
		rms = s[0]
		star = s[1].split("_st")[1].split("_")[0]
		ap = s[1].split("_ap")[1].split("/")[0]
		sortd.append((star,ap,rms))
	data = nar(sortd)
	numpy.save(rms_dir+"comb_lc_rms_beta.npy",data)


#read in eclipse depths for combined runs
print "\nReading eclipse depths from combined lightcurve outputs"
rms_dir = cmdir(output_dir+"rms/")
if os.path.exists(rms_dir+"comb_lc_ecl_beta.npy"):
	print "- Loading saved RMS data"
	ecl_data = numpy.load(rms_dir+"comb_lc_ecl_beta.npy")
else:
	bar = pbar(len(lc_folds)); cnt = 0;
	stats = []
	for f in lc_folds:
		bar.update(cnt); cnt +=1;
		fl = comb_lc_dir+f+phot_mod
		try:
			time = readfile(fl)[3]
		except IOError:
			stats.append((99999,f))
			continue
		if transit == True:
			res = readfile(fl)[3]
			input("THIS IS WORNG - GRUJSH EHSAOI")
		else:
			res = readfile(fl)[3]
		stats.append((numpy.amax(res)-numpy.amin(res),f))
	bar.finish()
	stats = sorted(stats)
	stats = nar(stats)
	sortd = []
	for s in stats:
		# print s
		rms = s[0]
		star = s[1].split("_st")[1].split("_")[0]
		ap = s[1].split("_ap")[1].split("/")[0]
		sortd.append((star,ap,rms))
	ecl_data = nar(sortd)
	numpy.save(rms_dir+"comb_lc_ecl_beta.npy",ecl_data)


#Plot ap,nstar vs [rms,eclipse] and calculate new eclipse depth
rms = data
RMS = zip(*rms)
RMS_c = numpy.asarray(RMS[2],dtype="float")
RMS_c = ((RMS_c/numpy.amin(RMS_c))*100)-100
RMS[2] = RMS_c
rms = zip(*RMS)
rms_arr = []
for r in rms:
	nstar = len(r[0].split("+"))
	rms_arr.append((r[2],nstar,r[1],r[0]))

cols = ["rms","n","ap","comb"]
df = pandas.DataFrame(data=rms_arr,columns=cols)
aps = df.ap.unique()
nstars = df.n.unique()

best = df.loc[df["rms"]<=rms_cut]

x,y,z = [],[],[]
for ap in aps:
	for N in nstars:
		try:
			z.append(df.loc[(df["ap"] == ap) & (df["n"] == N), "rms"].values[0])
		except IndexError:
			z.append(numpy.nan)
		x.append(float(N))
		y.append(float(ap))

arrays_to_file([x,y,z],output_dir+"rms.dat")
fig1 = plt.figure(figsize=(18,10))
rms_ax, rms_x,rms_y,rms_flux = plot_rms(x,y,z,fig1=fig1)
contour(rms_ax, rms_x,rms_y,rms_flux)

rms = ecl_data
RMS = zip(*rms)
RMS_c = numpy.asarray(RMS[2],dtype="float")
RMS[2] = RMS_c
rms = zip(*RMS)
rms_arr = []
for r in rms:
	nstar = len(r[0].split("+"))
	rms_arr.append((r[2],nstar,r[1],r[0]))

cols = ["rms","n","ap","comb"]
df = pandas.DataFrame(data=rms_arr,columns=cols)
aps = df.ap.unique()
nstars = df.n.unique()

x,y,z = [],[],[]
for ap in aps:
	for N in nstars:
		try:
			z.append(df.loc[(df["ap"] == ap) & (df["n"] == N), "rms"].values[0])
		except IndexError:
			z.append(numpy.nan)
		x.append(float(N))
		y.append(float(ap))
arrays_to_file([x,y,z],output_dir+"eclipse_depth.dat")
rms_ax, n,n,n = plot_ecl(x,y,z,fig1=fig1)
contour(rms_ax, rms_x,rms_y,rms_flux)

plt.savefig(output_dir+"ap_vs_star.pdf",filetype="pdf")
plt.savefig(output_dir+"ap_vs_star.eps",filetype="eps")


fig,axs=plt.subplots(3,4,sharex=True,sharey=True,figsize=(18,10))
ecl_depths = numpy.array([])
C1=0;C2=0;cnt=0;
for b in best.values:
	cnt+=1;
	if C1>=3:
		C1=0
		C2+=1
	ecl_col = readfile(comb_lc_dir+"lc_"+str(b[1])+"N_st"+str(b[3])+"_ap"+str(b[2])+"/"+channel_name)[col_no]
	ecl_depths = numpy.append(ecl_depths,ecl_col)
	if cnt<=12:
		pd = readfile(comb_lc_dir+"lc_"+str(b[1])+"N_st"+str(b[3])+"_ap"+str(b[2])+"/"+phot_output)
		md = readfile(comb_lc_dir+"lc_"+str(b[1])+"N_st"+str(b[3])+"_ap"+str(b[2])+"/"+phot_mod)
		mt = md[0]
		mf = md[3]
		ecld=numpy.amax(mf)-numpy.amin(mf)
		pt = pd[1]
		pflx = (pd[2]/pd[4])-ecld
		bt,bf,be = lcbin(pt,pflx,3)
		axs[C1][C2].plot(bt,bf,"o",color="k")
		errorbar(bt,bf,be,axs[C1][C2],cap=3)
		if C1!=0:
			axs[C1][C2].plot(mt,mf,"-",color="#FF5900",lw=5,alpha=.7)
			axs[C1][C2].axis(xmin=0.45,xmax=0.55,ymin=0.9971,ymax=1.0019)
		else:
			ax2 = axs[C1][C2].twiny()
			ax2.plot(mt,mf,"-",color="#FF5900",lw=5,alpha=.7)
			ax2.axis(xmin=0.45,xmax=0.55,ymin=0.9971,ymax=1.0019)
		if C2==3:
			ax3 = axs[C1][C2].twinx()
			ax3.plot(0.5,1,"+",alpha=0)
			ax3.axis(xmin=0.45,xmax=0.55,ymin=0.9971,ymax=1.0019)
		if C2 == 0 and C1==1:		axs[C1][C2].set_ylabel("Normalised Flux Ratios",fontsize=18)
		C1+=1
fig.text(0.5, 0.04, 'Phases', ha='center',fontsize=18)
plt.tight_layout()
plt.subplots_adjust(bottom=0.1)

plt.savefig(output_dir+"best_eclipses.pdf",filetype="pdf")
plt.savefig(output_dir+"best_eclipses.eps",filetype="eps")

plt.figure()
plt.hist(ecl_depths,20)
ecl_dep=numpy.median(ecl_depths)
ecl_dep_err=numpy.std(ecl_depths)
priln("*")
print "Eclipse Depth:", ecl_dep*100,"+/-",ecl_dep_err*100, "%"
priln("*")
plt.axvline(ecl_dep)
plt.axvline(ecl_dep+ecl_dep_err)
plt.axvline(ecl_dep-ecl_dep_err)
print "Done!"
plt.show()