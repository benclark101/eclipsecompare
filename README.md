# EclipseCompare

A program to determine the effet of aperture size and reference star selection on eclipse depth and timing when doing comparative time-series photometry.

## Requirements

This routine requries the use of the Markov Chain Monte Carlo code of https://doi.org/10.1111/j.1365-2966.2007.12195.x

Python 2.7
You'll also need the following libraries:
* uncertainties 
* numpy 
* matplotlib
* scipy
* commands
* subprocess
* multiprocessing
* signal
* pandas
* astropysics


## Usage

Download the raw python file to a destination of your choosing, and edit the "User Variables" section at the top of the .py file:

```############ USER VARIABLES ############
# User Variables ---------------------------------------------------------------
star_dir = "/media/bjc/Processing/nu_w48/phots_4/pre_filt/fw_None/" #output folder from phot code
output_dir = "/media/bjc/Processing/nu_w48/phots_4/runs/run2_backup/" #output folder

target = 1 #star number of target (usually 1)
sig_cut = 6 #sigma cut level
phot_output = "pltsecl06_01.dat" #the photometry file output by the mcmc for the specific eclipse
phot_mod = "pltsmod06.dat"
channel_name = "ch6.csv" #output csv file containing eclipse depth
rms_cut = 15 #% above minimum rmsbeta for LCs
script_dir = "/home/bjc/desk/cfht/lcfit/main/mcmc_inputs/" #path with mcmc inputs
col_no = 1 #output csv eclipse depth channel
mcmc_name = "mcmc_2016_occ2" #mcmc input scipt name
mcmc_path = script_dir+mcmc_name
mcmc_inp = script_dir+"linT.inp" #base mcmc input file
transit = False #is this a transit(True) or occultation(False)
max_stars=15 #maximum number of stars included in the final lightcurve
```

